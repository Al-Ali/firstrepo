#README


##README: general information

* Die App Badestellenfinder zeigt alle Schwimmbäder in der Nähe an.

* Der Author ist Christian Loell.

* Die App greift auf die Open Data der Berliner Bäderbetriebe zu.


##README: get startet

* Da es noch keine vollwertige Version im Playstore gibt,
 muss man die App mithilfe von Android Studios öffnen.
 [Link-Text]https://developer.android.com/studio/index.html

* Für die App benötig man die mindest SDK 15, aber man sollte
 die SDK 23 besitzen um alle Funtionen nutzen zukönnen.
 Diese SDK kann man im Android Studio SDK Manager Downloaden.


##README: test

* Im Verzeichnis Test befinden sich die Testmethoden für:
    * den Parser, der eine JSon Datei umwandelt
    * die Radiussuche, die alle Schwimmbäder in der Umgebung anzeigt
    * den Filter, der die Suche einschränkt